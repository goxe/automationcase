Feature: BDD


  @test
  Scenario: Automation Case

    #Chrome ile url'e gider
    When Open the Chrome and go to n11HomePage

    #Url'i kontrol eder
    And Check n11HomePage url

    #Cikan pop-up'i kapatir
    And Click ozelTeslimatKapat element

    #Giris yap butonuna click eder
    And Click girisYap element

    #Verilen kullanici bilgileriyle giris yapar
    And Login with credentials
      | goktuge1@gmail.com | Test123* |

    #Arama alanina Samsun yazar
    And Fill searchData with "Samsung"

    #Arama butonuna click eder
    And Click searchButton element

    #Samsung search url'ini kontrol eder
    And Check searchSamsungUrl url

    #Arama sonuclari sayfasinda 2. sayfaya gider
    And Click samsungSayfa2 element

    #2. sayfa url'ini kontrol eder
    And Check samsungSayfa2Url url

    #Acilan sayfada(sayfa 2) urun pozisyonu ile ustten 3. urune gider, favorilere ekler ve id'sini kontrol icin bir text dosyasina yazar
    And Get "id" field for productDataPosition and click favorilereEkleUrun3 element

    #Hesap seceneklerini hover eder ve istek listesine t�klar
    And Expand hesabimList element and click istekListemFavoriler

    #Acilan sayfada favorilerim'e click eder
    And Click favorilerim element

    #Acilan sayfadaki(favorilerim) urunun id'si ile text dosyasina alinan degeri kontrol eder
    And Get "id" field for favoriUrunDataCtgid and check with controlText

    #Favori urunu siler
    And Click favoriUrunSil element

    #Urun silindikten sonra cikan diyalogu kontrol eder
    And urunSilindiDialog element contains urunSilindi

    #Urun silindikten sonra cikan diyalogtaki Tamam'a click eder
    And Click urunSilindiTamam element

    #Id'si kaydedilen urunun izleme listesinde olmadigini dogrular
    And "controlText" element is not displayed

    #Browser'i kapatir
    Then Close browser

