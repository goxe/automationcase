package Library;

import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {

    public static void captureSecreenshot(WebDriver driver) {

        try {

            Date now = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd - HH mm ss");
            String time = dateFormat.format(now);

            Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
            ImageIO.write(fpScreenshot.getImage(), "PNG", new File(System.getProperty("user.dir") + "\\src\\test\\Logs\\Snapshots\\" + time + ".png"));

            System.out.println("Hata sonucu ekran goruntusu basariyla alidi! Dosya adi:" + time);
        } catch (Exception e) {
            System.out.println("Ekran goruntusu alinirken hata olustu!" + e.getMessage());
        }
    }


    public static void logger(String input) {

        BufferedWriter out = null;
        try {
            out = new BufferedWriter
                    (new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir") + "\\src\\test\\Logs\\ControlTexts\\ControlText.txt")));

            out.write(input);

        } catch (IOException e) {
            System.out.println("Hata:" + e.getMessage());
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (IOException e) {
                System.out.println("Hata2" + e.getMessage());
            }
        }
    }
}
