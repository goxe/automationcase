package Steps;

import Library.Utility;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class StepDefinition {

    private static WebDriver driver;

    @Before
    public void kill_chrome() throws IOException {

    Runtime killChromeDriver = Runtime.getRuntime();
    killChromeDriver.exec("taskkill /im chromedriver.exe /f /t");

    }

    // Browser driver'i ile URL'e gider

    @When("^Open the Firefox and go to ([^\"]*)$")
    public void open_the_Firefox_and_go_to_Url(String word) throws Throwable {

        WebDriverManager.firefoxdriver().setup();  // Firefox driver set

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        obj.load(objfile);

        try {
            driver = new FirefoxDriver();   //  Driver'a firefox driver set
            driver.manage().window().maximize(); // Pencere maksimize edilir
            driver.get(obj.getProperty(word));  //  Verilen url'e gider

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);  //  Hata ekran goruntusunu alir ve ...\src\test\Logs\Snapshots yoluna tarih ile kaydeder
            throw new Throwable();
        }
    }

    @When("^Open the Chrome and go to ([^\"]*)$")
    public void open_the_Chrome_and_go_to_Url(String word) throws Throwable {

        WebDriverManager.chromedriver().setup();   // Chrome driver set

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");  // properties dosyasini stream eder
        obj.load(objfile);
        try {
            driver = new ChromeDriver();   //  Driver'a chrome driver set
            driver.manage().window().maximize();  // Pencere maksimize edilir
            driver.get(obj.getProperty(word));  //  Verilen url'e gider
        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


    // DataTable'a set edilen creds ile giris yapar
    @When("^Login with credentials$")
    public void login_with_creds(DataTable creds) throws Throwable {

        List<String> list = creds.asList(String.class);
        try {
            driver.findElement(By.id("email")).sendKeys(list.get(0));
            driver.findElement(By.id("password")).sendKeys(list.get(1));
            driver.findElement(By.id("loginButton")).click();

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


// Elementin icindeki degeri kontrol eder

    @And("^([^\"]*) element contains ([^\"]*)")
    public void XPath_contains_text(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        InputStreamReader inputStreamReader = new InputStreamReader(objfile, "ISO-8859-9");   // Enocing formati verilir
        Properties obj = new Properties();
        obj.load(inputStreamReader);

        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(obj.getProperty(word)), obj.getProperty(word2)));  // Elementin icindeki degerin gorunur olmasini bekler


            driver.findElement(By.xpath(obj.getProperty(word) + "[text()[contains(.," + "'" + obj.getProperty(word2) + "'" + ")]]"));  // Elementi bulup icerigini kontrol eder (xPath ile)

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }

    @And("^([^\"]*) element contains \"([^\"]*)\"$")
    public void XPath_contains_string(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        InputStreamReader inputStreamReader = new InputStreamReader(objfile, "ISO-8859-9");   // Enocing formati verilir
        Properties obj = new Properties();
        obj.load(inputStreamReader);

        try {

            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(obj.getProperty(word)), word2));  // Elementin icindeki degerin gorunur olmasini bekler

            driver.findElement(By.xpath(obj.getProperty(word) + "[text()[contains(.," + "'" + word2 + "'" + ")]]")); // Elementi bulup icerigini kontrol eder (xPath ile)

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


    //    <Parametric object based click>

    @And("^Click ([^\"]*) element$")
    public void click_setXPath_button(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {

            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(obj.getProperty(word))));  //  xPath'in click edilebilir olmasini bekler

            driver.findElement(By.xpath(obj.getProperty(word))).click();   // Properties dosyasindan al�nan xPath'e click eder

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


    @And("^Click ([^\"]*) id$")
    public void click_setID_button(String word) throws Throwable {

        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.elementToBeClickable(By.id(word)));  //  Id'nin click edilebilir olmasini bekler

            driver.findElement(By.id(word)).click();  // Feature dosyasinda verilen id'ye click eder

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


    //    <Dynamic object based fill with element>


    @And("^Fill ([^\"]*) with \"([^\"]*)\"$")
    public void fill_id_with_string(String word, String word2) throws Throwable {

        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(word)));   // Elementin gorunur olmasini bekler

            driver.findElement(By.id(word)).sendKeys(word2);   //  Elementin icine verilen degerleri set eder

        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }

    }


    //  Url'i kontrol eder

    @And("^Check ([^\"]*) url$")
    public void check_Url(String word) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.urlContains(obj.getProperty(word)));  // Url'in gorunur olmasini bekler

            String URL = driver.getCurrentUrl();    //  Adres bardaki url'i getirir
            Assert.assertEquals(URL, obj.getProperty(word));  //  Getirilen url ile bekleneni kontrol eder
        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }



    @And("^Expand ([^\"]*) element and click ([^\"]*)$")
    public void check_Url(String word, String word2) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        try {
            Actions builder = new Actions(driver);

            WebElement element = driver.findElement(By.xpath(obj.getProperty(word)));
            builder.moveToElement(element);      //  Elementi hover eder

            WebElement subElement = driver.findElement(By.xpath(obj.getProperty(word2)));
            builder.moveToElement(subElement);
            builder.click().build().perform();   //  Sub elemente click eder


        } catch (Exception e) {
            Utility.captureSecreenshot(driver);
            throw new Throwable();
        }
    }


    @And("^Get \"([^\"]*)\" field for ([^\"]*) and click ([^\"]*) element$")
    public void get_field_for_element(String word, String word2, String word3) throws Throwable {

        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        Properties obj = new Properties();
        obj.load(objfile);


        List<WebElement> elements = driver.findElements(By.xpath(obj.getProperty(word2)));
        Iterator<WebElement> itr = elements.iterator();    //  Liste edilen elementleri yineler


        while (itr.hasNext()) {
            String input = itr.next().getAttribute(word);   // Getirilen elementlerin niteligine gider

            Utility.logger(input);
            Thread.sleep(8000);

            try {
                driver.findElement(By.xpath(obj.getProperty(word3))).click();   //  Verilen degerlere gore elemente click eder
            } catch (Exception e) {

                Utility.captureSecreenshot(driver);
                throw new Throwable();
            }
        }
    }


    @And("^Get \"([^\"]*)\" field for ([^\"]*) and check with ([^\"]*)$")
    public void XPath_contains_string_GetAccountBalance(String word, String word2, String word3) throws Throwable {

        String words;

        Properties obj = new Properties();
        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        obj.load(objfile);

        File file = new File(System.getProperty("user.dir") + "\\" + obj.getProperty(word3));  //  Dosya set edilir
        FileReader fileReader = new FileReader(file);   // File reader set
        BufferedReader bufferedReader = new BufferedReader(fileReader);       // Buffered file reader

        List<WebElement> elements = driver.findElements(By.xpath(obj.getProperty(word2)));
        Iterator<WebElement> itr = elements.iterator();

        //  Dosyay� okur (string)
        while ((words = bufferedReader.readLine()) != null & itr.hasNext()) {

            String input = itr.next().getAttribute(word);   // Getirilen elementlerin niteligine gider

            if (input.equals(words)) {  //  Getirilen deger ile dosyadaki ayni oldugunu kontrol eder

                System.out.println("Favori urun ile izleme listesindeki urun aynidir.");

            } else {

                System.out.println("Favori urun ile izleme listesindeki urun farklidir. Favori urun :" + words + "  |  Izleme listesindeki urun : " + input);
                throw new Throwable();
            }
        }
    }


    @And("^\"([^\"]*)\" element is not displayed$")
    public void element_is_not_displayed(String word) throws Throwable {


        FileInputStream objfile = new FileInputStream(System.getProperty("user.dir") + "\\src\\test\\resources\\Properties\\tweezers.properties");
        Properties obj = new Properties();
        obj.load(objfile);

        File file = new File(System.getProperty("user.dir") + "\\" + obj.getProperty(word));
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String words;


        while ((words = bufferedReader.readLine()) != null) {

            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//div[@id=" + "'" + words + "']")));   // Elementin kaybolmasini bekler

            List<WebElement> elements = driver.findElements(By.xpath(".//div[@id=" + "'" + words + "']"));   //  Verilen degerler ile elementleri listeler
            if (elements.isEmpty()) {   //  Listeleme sonucunun bos oldugunu kontrol eder
                System.out.println();

            } else {
                System.out.println(words + " element is displayed!");
                Utility.captureSecreenshot(driver);
                throw new Throwable();

            }
        }
    }


    @And("^Close browser$")
    public void close_Browser() {

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        driver.close();  //  Browser'i kapatir

    }

}
